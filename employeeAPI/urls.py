from django.urls import path
from . import views


urlpatterns = [
    path('',views.EmployeeAPI),
    path('api/emp-list/',views.EmployeeList,name='emplist'),
    path('api/emp-create/',views.EmployeeCreate,name='empcreate'),
    path('api/emp-details/<str:pk>/',views.EmployeeDetails,name='empdetails'),
    path('api/emp-update/<str:pk>/',views.EmployeeUpdate,name='empupdate'),
    path('api/emp-delete/<str:pk>/',views.EmployeeDelete,name='empdelete'),
]

from django.db import models

#create employee model
class Employee(models.Model):
    emp_id=models.IntegerField(primary_key=True)
    emp_name=models.CharField(max_length=20)
    emp_designation=models.CharField(max_length=20)

    def __str__(self):
        return self.emp_name    
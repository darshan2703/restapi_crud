from django.shortcuts import redirect
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import *
from .models import *


#API overview
@api_view(['GET'])
def EmployeeAPI(request):
    api_urls={
        'Get All Employee Data':"/api/emp-list/",
        'Create Employee':"/api/emp-create/",
        'Get Employee Details':"/api/emp-details/<str:pk>/",
        'Update Employee Data':"/api/emp-update/<str:pk>/",
        'Delete Employee Data':"/api/emp-delete/<str:pk>/"
    }
    return Response(api_urls)

#get all employee data
@api_view(['GET'])
def EmployeeList(request):
    employees=Employee.objects.all()
    serialize_emp=EmployeeSerializer(employees,many=True)
    return Response(serialize_emp.data)

#get perticular employee data 
@api_view(['GET'])
def EmployeeDetails(request,pk):
    employees=Employee.objects.filter(emp_id=pk)
    serialize_emp=EmployeeSerializer(employees,many=True)
    return Response(serialize_emp.data)

#create new employee record
@api_view(['POST'])
def EmployeeCreate(request):
    serialize_emp=EmployeeSerializer(data=request.data)
    if serialize_emp.is_valid():
        serialize_emp.save()
    return redirect('emplist')
    
#update employee record
@api_view(['POST'])
def EmployeeUpdate(request,pk):
    employees=Employee.objects.get(emp_id=pk)
    serialize_emp=EmployeeSerializer(instance=employees,data=request.data)
    if serialize_emp.is_valid():
        serialize_emp.save()
    return Response(serialize_emp.data)

#delete employee record
@api_view(['DELETE'])
def EmployeeDelete(request,pk):
    employees=Employee.objects.get(emp_id=pk)
    employees.delete()
    return Response("Data Delete Successfully...")